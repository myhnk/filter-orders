/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
"use strict";

var gOrderDB = {
    orders: [],
    //phương thức lọc của Obj
    filterOrder: function (paramFilterObj) {
        var vFilterArr = this.orders.filter(function (order) {
            return ((order.trangThai === paramFilterObj.trangThai || paramFilterObj.trangThai === "none")
                && (order.loaiPizza === paramFilterObj.loaiPizza || paramFilterObj.loaiPizza === "none"))
        });
        return vFilterArr;
    }
};

const gORDER_COL = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
const gCOLUMN_ORDER_ID = 0;
const gCOLUMN_KICH_CO = 1;
const gCOLUMN_LOAI_PIZZA = 2;
const gCOLUMN_NUOC_UONG = 3;
const gCOLUMN_THANH_TIEN = 4;
const gCOLUMN_HO_TEN = 5;
const gCOLUMN_SO_DIEN_THOAI = 6;
const gCOLUMN_TRANG_THAI = 7;
const gCOLUMN_ACTION = 8;

// Tạo DataTable
var gOrderTable = $("#order-table").DataTable({
    columns: [
        { "data": gORDER_COL[gCOLUMN_ORDER_ID] },
        { "data": gORDER_COL[gCOLUMN_KICH_CO] },
        { "data": gORDER_COL[gCOLUMN_LOAI_PIZZA] },
        { "data": gORDER_COL[gCOLUMN_NUOC_UONG] },
        { "data": gORDER_COL[gCOLUMN_THANH_TIEN] },
        { "data": gORDER_COL[gCOLUMN_HO_TEN] },
        { "data": gORDER_COL[gCOLUMN_SO_DIEN_THOAI] },
        { "data": gORDER_COL[gCOLUMN_TRANG_THAI] },
        { "data": gORDER_COL[gCOLUMN_ACTION] }
    ],

    // định nghĩa các column
    columnDefs: [
        {
            targets: gCOLUMN_ACTION,
            defaultContent: `<button class="btn btn-primary btn-detail"><i class='fas fa-info'>&nbsp</i>Chi tiết</button>`
        }
    ]
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // sự kiện click nút chi tiết
    $("#order-table").on("click", ".btn-detail", function () {
        onBtnDetailClick(this);
    })
    // sự kiện click nút lọc
    $("#btn-filter").on("click", onBtnFilterOrderClick);
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện khi load trang
function onPageLoading() {
    "use strict";
    //B1: thu thập dữ liệu (không có)
    //B2: validate (không có)
    //B3: call API lấy danh sách đơn hàng
    $.ajax({
        url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
        type: "GET",
        dataType: 'json',
        success: function (res) {
            gOrderDB.orders = res;
            console.log(gOrderDB.orders);
            //B4: xử lý hiển thị load dữ liệu lên bảng
            loadDataToTable(gOrderDB.orders);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    })
}

// hàm xử lý sự kiện click nút chi tiết
function onBtnDetailClick(paramDetailBtn) {
    "use strict";
    var vRowSelected = $(paramDetailBtn).closest("tr");
    var vDataRow = gOrderTable.row(vRowSelected).data();
    console.log(vDataRow);
}

// hàm xử lý sự kiện click nút lọc order
function onBtnFilterOrderClick() {
    "use strict";
    var vOrderObj = {
        trangThai: "",
        loaiPizza: "",
    }
    //B1: thu thập dữ liệu
    getDataToSelect(vOrderObj);
    //B2: validate không có
    //B3: xử lý hiển thị dữ liệu đã được lọc trên table
    var vFilterOrder = gOrderDB.filterOrder(vOrderObj);
    // hiển thị dữ liệu lên bảng
    loadDataToTable(vFilterOrder);
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm thu thập dữ liệu
function getDataToSelect(paramOrderObj) {
    "use strict";
    paramOrderObj.trangThai = $("#select-status").val().trim();
    paramOrderObj.loaiPizza = $("#select-pizza").val().trim();
}

// hàm load dữ liệu đơn hàng lên bảng
function loadDataToTable(paramResponseObj) {
    "use strict";
    gOrderTable.clear();
    gOrderTable.rows.add(paramResponseObj);
    gOrderTable.draw();
}

